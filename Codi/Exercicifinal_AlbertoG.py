import pygame
import pygame.freetype
import random
# ニガー
FPS=60
WINDOW_SIZE_X=1438
WINDOW_SIZE_Y=810
GENERAR_EXPERIENCIA = 10
GENERAR_EXPERIENCIA_CLICANDO = 10
GENERAR_MINERALES = 10
GENERAR_MINERALES_CLICANDO= 10

# game states
GAME_STATE_MENU=1
GAME_STATE_PLAYING=2
GAME_STATE_RESULTS=3
GAME_STATE_EXIT=4


#------------------------
#---COSES-ESCRITES-JOC---
#------------------------
def draw_overlay(screen,font ,player):  
    font["overlay_font"].render_to(screen, (55, 13), '' + str(player["damage"]), (255,255,255))
    font["overlay_font"].render_to(screen, (53, 63), '' + str(player["potencia"]), (255,255,255))
    font["overlay_font"].render_to(screen, (295, 13), '' + str(player["experiencia"]), (255,255,255))
    font["overlay_font"].render_to(screen, (295, 63), '' + str(player["piedra"]), (255,255,255))


#---------------------------
#--COSES-ESCRITES-BOTIGUES--
#---------------------------
def draw_overlayb(screen, font, player,shop):
# Esto es los dos del medio
    font["overlay_font"].render_to(screen, (655, 85), '' + str(player["experiencia"]), (255,255,255))
    font["overlay_font"].render_to(screen, (655, 160), '' + str(player["piedra"]), (255,255,255))
# Daños Armas
    font["overlay_font2"].render_to(screen, (142, 350), '' + str(shop["SMadera"][0]), (255,255,255))
    font["overlay_font2"].render_to(screen, (290, 350), '' + str(shop["SOro"][0]), (255,255,255))
    font["overlay_font2"].render_to(screen, (440, 350), '' + str(shop["SPiedra"][0]), (255,255,255))
    font["overlay_font2"].render_to(screen, (142, 635), '' + str(shop["SHierro"][0]), (255,255,255))
    font["overlay_font2"].render_to(screen, (290, 635), '' + str(shop["SDiamanete"][0]), (255,255,255))
    font["overlay_font2"].render_to(screen, (440, 635), '' + str(shop["SNetherite"][0]), (255,255,255))
# Precios Armas
    font["overlay_font3"].render_to(screen, (115, 310), '' + str(shop["SMadera"][1])+ " = (" + str(player["damage"] + shop["SMadera"][1]) + ")", (255,255,255))
    font["overlay_font3"].render_to(screen, (265, 310), '' + str(shop["SOro"][1])+ " = (" + str(player["damage"] + shop["SOro"][1]) + ")", (255,255,255))
    font["overlay_font3"].render_to(screen, (415, 310), '' + str(shop["SPiedra"][1])+ " = (" + str(player["damage"] + shop["SPiedra"][1]) + ")", (255,255,255))
    font["overlay_font3"].render_to(screen, (115, 593), '' + str(shop["SHierro"][1])+ " = (" + str(player["damage"] + shop["SHierro"][1]) + ")", (255,255,255))
    font["overlay_font3"].render_to(screen, (265, 593), '' + str(shop["SDiamanete"][1])+ " = (" + str(player["damage"] + shop["SDiamanete"][1]) + ")", (255,255,255))
    font["overlay_font3"].render_to(screen, (415, 593), '' + str(shop["SNetherite"][1])+ " = (" + str(player["damage"] + shop["SNetherite"][1]) + ")", (255,255,255))
# Potencia Picos
    font["overlay_font2"].render_to(screen, (957, 350), '' + str(shop["PMadera"][0]), (255,255,255))
    font["overlay_font2"].render_to(screen, (1105, 350), '' + str(shop["POro"][0]), (255,255,255))
    font["overlay_font2"].render_to(screen, (1255, 350), '' + str(shop["PPiedra"][0]), (255,255,255))
    font["overlay_font2"].render_to(screen, (957, 635), '' + str(shop["PHierro"][0]), (255,255,255))
    font["overlay_font2"].render_to(screen, (1105, 635), '' + str(shop["PDiamanete"][0]), (255,255,255))
    font["overlay_font2"].render_to(screen, (1255, 635), '' + str(shop["PNetherite"][0]), (255,255,255))
# Precios Picos
    font["overlay_font3"].render_to(screen, (930, 310), '' + str(shop["PMadera"][1])+ " = (" + str(player["potencia"] + shop["PMadera"][1]) + ")", (255,255,255))
    font["overlay_font3"].render_to(screen, (1080, 310), '' + str(shop["POro"][1])+ " = (" + str(player["potencia"] + shop["POro"][1]) + ")", (255,255,255))
    font["overlay_font3"].render_to(screen, (1230, 310), '' + str(shop["PPiedra"][1])+ " = (" + str(player["potencia"] + shop["PPiedra"][1]) + ")", (255,255,255))
    font["overlay_font3"].render_to(screen, (930, 593), '' + str(shop["PHierro"][1])+ " = (" + str(player["potencia"] + shop["PHierro"][1]) + ")", (255,255,255))
    font["overlay_font3"].render_to(screen, (1080, 593), '' + str(shop["PDiamanete"][1])+ " = (" + str(player["potencia"] + shop["PDiamanete"][1]) + ")", (255,255,255))
    font["overlay_font3"].render_to(screen, (1230, 593), '' + str(shop["PNetherite"][1])+ " = (" + str(player["potencia"] + shop["PNetherite"][1]) + ")", (255,255,255))


#-----------------------
#----IMG-UTILITZADES----
#-----------------------
def img():
    imatges={
        "menu": pygame.image.load('images/menuprincipal.png'),
        "tutorial" : pygame.image.load('images/Tutorial/tutorial.png'),
        "tutorialmob" : pygame.image.load('images/Tutorial/mob tutorial.png'),
        "tutorialores" : pygame.image.load('images/Tutorial/ores tutorial.png'),
        "tipustria" : pygame.image.load("images/Tipus/tria.png"),
        "tria-potenciador" : pygame.image.load("images/Tipus/tria-potenciador.png"),
        "forest" :pygame.image.load('images/Game/Forest.jpg'),
        "mine" :pygame.image.load('images/Game/Mine.jpg'),
        "botiga" :pygame.image.load("images/Botiga/Botiga.png"),
        "final" :pygame.image.load("images/Final/Fondo.png"),
    }
    return imatges


#-----------------------
#---FONTS-UTILITZADES---
#-----------------------
def fuentes():
    font ={   
    "overlay_font"  : pygame.freetype.Font("fonts/Lato-Black.ttf", 40),#Fuente principal
    "overlay_font2" : pygame.freetype.Font("fonts/Lato-Black.ttf", 28),#Fuente de los precios
    "overlay_font3" : pygame.freetype.Font("fonts/Lato-Black.ttf", 20)#Fuente de los aumentos
    }
    return font


#--------------------------
#--MILLORA-DEL-PERSONATGE--
#--------------------------
def update_player(player, shop, potenciador):
    if potenciador == True:
        player['damage'] = 10 + \
            (shop["SMadera"][1] * shop["SMadera"][2] + 
            shop["SPiedra"][1] * shop["SPiedra"][2] + 
            shop["SOro"][1]    * shop["SOro"][2]    +
            shop["SHierro"][1] * shop["SHierro"][2] +
            shop["SDiamanete"][1] * shop["SDiamanete"][2] +
            shop["SNetherite"][1] * shop["SNetherite"][2] )* 10
        player['potencia']= 10 + \
            (shop["PMadera"][1] * shop["PMadera"][2] + 
            shop["PPiedra"][1] * shop["PPiedra"][2] + 
            shop["POro"][1]    * shop["POro"][2]    +
            shop["PHierro"][1] * shop["PHierro"][2] +
            shop["PDiamanete"][1] * shop["PDiamanete"][2] +
            shop["PNetherite"][1] * shop["PNetherite"][2] )* 10
    else:
        player['damage'] = 10 + \
            (shop["SMadera"][1] * shop["SMadera"][2] + 
            shop["SPiedra"][1] * shop["SPiedra"][2] + 
            shop["SOro"][1]    * shop["SOro"][2]    +
            shop["SHierro"][1] * shop["SHierro"][2] +
            shop["SDiamanete"][1] * shop["SDiamanete"][2] +
            shop["SNetherite"][1] * shop["SNetherite"][2])
        player['potencia']= 10 + \
            (shop["PMadera"][1] * shop["PMadera"][2] + 
            shop["PPiedra"][1] * shop["PPiedra"][2] + 
            shop["POro"][1]    * shop["POro"][2]    +
            shop["PHierro"][1] * shop["PHierro"][2] +
            shop["PDiamanete"][1] * shop["PDiamanete"][2] +
            shop["PNetherite"][1] * shop["PNetherite"][2])


#-----------------------
#-----CREATE-PLAYER-----
#-----------------------
def create_player(shop, potenciador):
    player ={
        "damage" : 0,
        "potencia" : 0,
        "experiencia" : 0,
        "piedra" : 0,
        "experencia_auto" : GENERAR_EXPERIENCIA,
        "potencia_auto" : GENERAR_MINERALES,
    }
    update_player(player,shop, potenciador)
    return player


#-----------------------
#------CREATOR-GIF------
#-----------------------
def windimg():
    finish=[]
    part1a="Final ("
    part2a=").gif"
    for n in range(1, 127):
        s=str(n)
        nom= part1a + s + part2a
        finish.append(pygame.image.load("images/Final/Finalizacion/" + nom))
    return finish


    pygame.display.flip()


#-----------------------
#-------GAME-PLAY-------
#-----------------------
def game_playing(screen,game,imatges):
    mobs ={#                   foto                                                 dmg    drop 
        "Endermite" : [pygame.image.load('images/Mobs/Ender Dragon.png'),           100,    80],
        "Ghast" : [pygame.image.load('images/Mobs/Ghast.png'),                      100,    80],
        "Silverfish" : [pygame.image.load('images/Mobs/silverfish.png'),            150,   125],
        "Blaze" : [pygame.image.load('images/Mobs/blaze.png'),                      200,   180],
        "Slime" : [pygame.image.load('images/Mobs/Slime.png'),                      200,   180],
        "Vex" : [pygame.image.load('images/Mobs/vex.png'),                          200,   180], 
        "Creeper" : [pygame.image.load('images/Mobs/Creeper.png'),                  250,   200],
        "Guardian" : [pygame.image.load('images/Mobs/guardian.png'),                250,   200],
        "Pillager" : [pygame.image.load('images/Mobs/Pilager.png'),                 250,   200], 
        "Magma cube" : [pygame.image.load('images/Mobs/Magma_Cube.png'),            250,   200],
        "Evoker" : [pygame.image.load('images/Mobs/Evoker.png'),                    250,   200],
        "Stray" : [pygame.image.load('images/Mobs/Stray.png'),                      300,   250],  
        "Phantom" : [pygame.image.load('images/Mobs/phantom.png'),                  300,   250],
        "Shulker" : [pygame.image.load('images/Mobs/Shulker.png'),                  300,   250],
        "Skeleton" : [pygame.image.load('images/Mobs/skeleton.png'),                300,   250],
        "Wither skeleton" : [pygame.image.load('images/Mobs/whiter skeleton.png'),  300,   250],
        "Zombie" : [pygame.image.load('images/Mobs/ZOMBIE.png'),                    300,   250],
        "Vindicator" : [pygame.image.load('images/Mobs/Vindicator.png'),            340,   300],
        "Witch" : [pygame.image.load('images/Mobs/which.png'),                      340,   300],
        "Ender Dragon" : [pygame.image.load('images/Mobs/ender mite.png'),          400,   350], 
        "Elder Guardian" : [pygame.image.load('images/Mobs/elder guardian.png'),    500,   450],
        "Piglin Brute" : [pygame.image.load('images/Mobs/Piglin Brute.png'),        500,   450],
        "Ravager" : [pygame.image.load('images/Mobs/Ravager.png'),                  1000,  850], 
        "Warden" : [pygame.image.load('images/Mobs/Warden.png'),                    1200,  950],
        "Whither" : [pygame.image.load('images/Mobs/whiter.png'),                   2000, 1450],
#   Son las listas para podedr hacer unos requisitos de menos nivel más abajo
        "mobspequeños" : ["Endermite","Ghast","Silverfish","Blaze","Creeper","Slime","Vex","Guardian"],
        "todosmobs" : ["Endermite","Ghast","Silverfish","Blaze","Creeper","Slime","Vex","Guardian","Pillager","Magma cube","Evoker","Stray","Phantom","Shulker","Skeleton","Wither skeleton","Zombie","Vindicator","Witch","Ender Dragon","Elder Guardian","Piglin Brute","Ravager","Warden","Whither"]
    }
    minerales = {#                   foto                               dmg     drop
        "coaldN" : [pygame.image.load('images/Ores/CoaldN.png'),        40,     30],
        "coaldO" : [pygame.image.load('images/Ores/CoaldO.png'),        80,     30],
        "cobreN" : [pygame.image.load('images/Ores/CobreN.png'),        40,     30],
        "cobreO" : [pygame.image.load('images/Ores/CobreO.png'),        80,     30],
        "nuggetsG" : [pygame.image.load('images/Ores/NuggetsG.png'),    120,    60],
        "cuarzo" : [pygame.image.load('images/Ores/Cuarzo.png'),        160,    100],
        "goldN" : [pygame.image.load('images/Ores/GoldN.png'),          240,    220],
        "ironN" : [pygame.image.load('images/Ores/IronN.png'),          240,    220],
        "lapisN" : [pygame.image.load('images/Ores/LapisN.png'),        360,    200],
        "lapisO" : [pygame.image.load('images/Ores/LapisO.png'),        400,    200],
        "redstoneO" : [pygame.image.load('images/Ores/RedstoneO.png'),  360,    200],
        "redstoneN" : [pygame.image.load('images/Ores/RedstonN.png'),   400,    200],
        "goldO" : [pygame.image.load('images/Ores/GoldO.png'),          300,    220],
        "ironO" : [pygame.image.load('images/Ores/IronO.png'),          300,    220],
        "diamonN" : [pygame.image.load('images/Ores/DiamonN.png'),      400,    300],
        "diamonO" : [pygame.image.load('images/Ores/DiamonO.png'),      440,    300],
        "esmeraldN" : [pygame.image.load('images/Ores/EsmeraldN.png'),  400,    300],
        "esmeraldO" : [pygame.image.load('images/Ores/EsmeraldO.png'),  440,    300],
        "netherite" : [pygame.image.load('images/Ores/Netherite.png'),  3000,   1000],
#   Son las listas para podedr hacer unos requisitos de menos nivel más abajo
        "mineralespequeños" : ["coaldN","coaldO","cobreN","cobreO","nuggetsG","cuarzo","ironN","goldN"],
        "todosminerales" :["coaldN","coaldO","cobreN","cobreO","diamonN","diamonO","esmeraldN","esmeraldO","goldN","goldO","ironN","ironO","lapisN","lapisO","nuggetsG","cuarzo","redstoneO","redstoneN","netherite"]
        }
    shop ={#             COST   DMG  U                FOTO 
        "sortir" :                      pygame.Rect(570 , 340 , 228 , 61),
        "menu" :                        pygame.Rect(570 , 418 , 228 , 61),
        "SMadera" :     [750  , 30 , 0, pygame.Rect(107 , 342 , 107 , 41)],
        "SOro" :        [1500 , 40 , 0, pygame.Rect(255 , 342 , 107 , 41)],
        "SPiedra" :     [1700 , 50 , 0, pygame.Rect(403 , 342 , 107 , 41)],
        "SHierro" :     [3000 , 60 , 0, pygame.Rect(107 , 628 , 107 , 41)],
        "SDiamanete" :  [3750 , 70 , 0, pygame.Rect(255 , 628 , 107 , 41)],
        "SNetherite" :  [4500 , 80 , 0, pygame.Rect(403 , 628 , 107 , 41)],
        "PMadera" :     [500  , 30 , 0, pygame.Rect(920 , 342 , 107 , 41)],
        "POro" :        [1250 , 40 , 0, pygame.Rect(1068 , 342 , 107 , 41)],
        "PPiedra" :     [1500 , 50 , 0, pygame.Rect(1216 , 342 , 107 , 41)],
        "PHierro" :     [2250 , 60 , 0, pygame.Rect(920 , 628 , 107 , 41)],
        "PDiamanete" :  [3000 , 70 , 0, pygame.Rect(1068 , 628 , 107 , 41)],
        "PNetherite" :  [4000 , 80 , 0, pygame.Rect(1216 , 628 , 107 , 41)]
    }
    potenciador = False
    player = create_player(shop, potenciador)
    font = fuentes()
    end_sq = pygame.Rect( 285 , 143 , 203 , 56)
    tipus_sq = pygame.Rect( 0 , 143 , 203 , 56)
    botiga_sq = pygame.Rect( 0 , 384 , 71 , 145)
    mineral_sq = pygame.Rect(670,390, 214, 213)
    mob_sq = pygame.Rect(650,210,650,480)
    clock = pygame.time.Clock()
    elapsed_time=0
    countm = 0
    countb = 0
    actual = 1
    timer = 90
    win = False
    botiga = False
    going=True
    tipus = False
    winanimation = False
    mob_escogido = random.choice(mobs["mobspequeños"])
    mineral_escogido = random.choice(minerales["mineralespequeños"])
    ptc = 0
    dmg = 0
    acabar = windimg()
    print(mineral_escogido, minerales[mineral_escogido][0])
    while going:
        delta=clock.tick(FPS)
        elapsed_time += delta
        if elapsed_time >= 1000:
            if player["experiencia"] >= 9999999:
                player["experiencia"] = 9999999
            else:
                player["experiencia"] += GENERAR_EXPERIENCIA
            if player["piedra"] >= 9999999:
                player["piedra"] = 9999999
            else:
                player["piedra"] += GENERAR_MINERALES
            elapsed_time = 500
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                going = False
                result = GAME_STATE_EXIT
            if win == False:
                if botiga == False:
# Aquest són el clicks que hi tenim dintre del joc:
# per canviar de joc, sortir, botiga
                    if event.type == pygame.MOUSEBUTTONDOWN:
                        if tipus==False:             
                            if tipus_sq.collidepoint(pygame.mouse.get_pos()):
                                tipus = True
                            if end_sq.collidepoint(pygame.mouse.get_pos()):
                                going = False
                                result = GAME_STATE_EXIT
                            if botiga_sq.collidepoint(pygame.mouse.get_pos()):
                                botiga = True
                        else:
# Escolli el tipus de joc i el potenciador
                            if tipus_ores.collidepoint(pygame.mouse.get_pos()):
                                tipus = False
                                game["tipus"] =  1
                            elif tipus_mob.collidepoint(pygame.mouse.get_pos()):
                                tipus = False
                                game["tipus"] =  2
                            elif tipus_potenciador.collidepoint(pygame.mouse.get_pos()):
                                potenciador = True
                                tipus = False
                            else:
                                tipus = False
                        if mineral_sq.collidepoint(pygame.mouse.get_pos()):
                            ptc += player["potencia"]
                        if mob_sq.collidepoint(pygame.mouse.get_pos()):
                            dmg += player["damage"]
# Aquesta es la part de clicks per poder trencar / matar el mob o el mineral
# Aqui tens la part de minerals 
            if game["tipus"] == 1:
                    screen.blit(imatges["mine"], imatges["mine"].get_rect())
                    draw_overlay(screen, font, player)
                    if ptc >= minerales[mineral_escogido][1] and countb <= 20:
                        mineral_escogido = random.choice(minerales["mineralespequeños"])
                        print(mineral_escogido, minerales[mineral_escogido][0])
                        ptc = 0
                        if potenciador == True:
                            player["piedra"] += minerales[mineral_escogido][2] * 10
                        else:
                            player["piedra"] += minerales[mineral_escogido][2]
                        countb += 1
                    if ptc >= minerales[mineral_escogido][1]:
                        mineral_escogido = random.choice(minerales["todosminerales"])
                        print(mineral_escogido, minerales[mineral_escogido][0])
                        ptc = 0
                        if potenciador == True:
                            player["piedra"] += minerales[mineral_escogido][2] * 10
                        else:
                            player["piedra"] += minerales[mineral_escogido][2]
                    screen.blit(minerales[mineral_escogido][0], minerales[mineral_escogido][0].get_rect().move(670,390))
# Aquí tens la part de mobs
            elif game["tipus"] == 2:
                    screen.blit(imatges["forest"], imatges["forest"].get_rect())
                    draw_overlay(screen, font, player)

                    if dmg >= mobs[mob_escogido][1] and countm <= 20:
                        mob_escogido = random.choice(mobs["mobspequeños"])
                        print(mob_escogido, mobs[mob_escogido][0])
                        dmg = 0
                        if potenciador == True:
                            player["experiencia"] += mobs[mob_escogido][2] * 10
                        else:
                            player["experiencia"] += mobs[mob_escogido][2]
                        countm += 1
                    if dmg >= mobs[mob_escogido][1]:
                        mob_escogido = random.choice(mobs["todosmobs"])
                        print(mob_escogido, mobs[mob_escogido][0])
                        dmg = 0
                        if potenciador == True:
                            player["experiencia"] += mobs[mob_escogido][2] * 10
                        else:
                            player["experiencia"] += mobs[mob_escogido][2]
                    screen.blit(mobs[mob_escogido][0], mobs[mob_escogido][0].get_rect().move(650,200)) 
# Aquí comença la botiga
            if botiga:
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if shop["SMadera"][3].collidepoint(pygame.mouse.get_pos()):
                        if  player["experiencia"] >= shop["SMadera"][0]:
                            player["experiencia"] -= shop["SMadera"][0]
                            shop["SMadera"][2] += 1
                    if shop["SPiedra"][3].collidepoint(pygame.mouse.get_pos()):
                        if  player["experiencia"] >= shop["SPiedra"][0]:
                            player["experiencia"] -= shop["SPiedra"][0]
                            shop["SPiedra"][2] += 1
                    if shop["SOro"][3].collidepoint(pygame.mouse.get_pos()):
                        if  player["experiencia"] >= shop["SOro"][0]:
                            player["experiencia"] -= shop["SOro"][0]
                            shop["SOro"][2] += 1
                    if shop["SHierro"][3].collidepoint(pygame.mouse.get_pos()):
                        if  player["experiencia"] >= shop["SHierro"][0]:
                            player["experiencia"] -= shop["SHierro"][0]
                            shop["SHierro"][2] += 1
                    if shop["SDiamanete"][3].collidepoint(pygame.mouse.get_pos()):
                        if player["experiencia"] >= shop["SDiamanete"][0]:
                            player["experiencia"] -= shop["SDiamanete"][0]
                            shop["SDiamanete"][2] += 1
                    if shop["SNetherite"][3].collidepoint(pygame.mouse.get_pos()):
                        if player["experiencia"] >= shop["SNetherite"][0]:
                            player["experiencia"] -= shop["SNetherite"][0]
                            shop["SNetherite"][2] += 1
                    if shop["PMadera"][3].collidepoint(pygame.mouse.get_pos()):
                        if  player["piedra"] >= shop["PMadera"][0]:
                            player["piedra"] -= shop["PMadera"][0]
                            shop["PMadera"][2] += 1
                    if shop["PPiedra"][3].collidepoint(pygame.mouse.get_pos()):
                        if player["piedra"] >= shop["PPiedra"][0]:
                            player["piedra"] -= shop["PPiedra"][0]
                            shop["PPiedra"][2] += 1
                    if shop["POro"][3].collidepoint(pygame.mouse.get_pos()):
                        if  player["piedra"] >= shop["POro"][0]:
                            player["piedra"] -= shop["POro"][0]
                            shop["POro"][2] += 1
                    if shop["PHierro"][3].collidepoint(pygame.mouse.get_pos()):
                        if  player["piedra"] >= shop["PHierro"][0]:
                            player["piedra"] -= shop["PHierro"][0]
                            shop["PHierro"][2] += 1
                    if shop["PDiamanete"][3].collidepoint(pygame.mouse.get_pos()):
                        if  player["piedra"] >= shop["PDiamanete"][0]:
                            player["piedra"] -= shop["PDiamanete"][0]
                            shop["PDiamanete"][2] += 1
                    if shop["PNetherite"][3].collidepoint(pygame.mouse.get_pos()):
                        if  player["piedra"] >= shop["PNetherite"][0]:
                            player["piedra"] -= shop["PNetherite"][0]
                            shop["PNetherite"][2] += 1
                    if shop["menu"].collidepoint(pygame.mouse.get_pos()):
                        botiga = False
                    update_player(player,shop, potenciador)
                screen.blit(imatges["botiga"], imatges["botiga"].get_rect().move(35,0))  
                draw_overlayb(screen, font, player,shop)
            if winanimation:
                sortida = pygame.Rect( 540 , 650 , 340 , 90)    
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if sortida.collidepoint(pygame.mouse.get_pos()):
                        going = False
                        result = GAME_STATE_EXIT
                screen.blit(imatges["final"],imatges["final"].get_rect())
# Aquí acaba la botiga
# Aquí comença el tipus per si vols canviar de mode  de joc i on està el potenciador
        if tipus:
            tipus_ores = pygame.Rect( 185 , 330 , 294 , 316)
            tipus_mob = pygame.Rect( 1058 , 285 , 202 , 333)
            tipus_potenciador = pygame.Rect(530 , 720 , 300 , 85)
            screen.blit(imatges["tria-potenciador"], imatges["tria-potenciador"].get_rect())
# Aquí el escollir el tipus
# Aquí podem veure les condicions per poder acabar el joc  
        if player["damage"] >= 1000000 or player["experiencia"] >= 1000000 or player["piedra"] >= 1000000 or player["potencia"] >= 1000000:
            win = True
        if player["damage"] >= 1000000 or player["experiencia"] >= 1000000 or player["piedra"] >= 1000000 or player["potencia"] >= 1000000:
            win = True
# Comença el .gif del final que és el final de Minecraft de veritat
        if win == True and winanimation==False:
            screen.blit(acabar[actual],acabar[actual].get_rect())
            timer -= delta
            if timer <= 0:
                timer += 90
                actual += 1
            if actual == 126:
                winanimation = True
                actual = 1
# Pantalla final del joc on podras sortir
        pygame.display.flip()
    return result


#-----------------------
#-------GAME-MENU-------
#-----------------------
def game_menu(screen, game,imatges):
    clock = pygame.time.Clock()
    play_sq = pygame.Rect( 175 , 428 , 280 , 80)
    end_sq = pygame.Rect( 175 , 585 , 280 , 80)
    tutorial_sq = pygame.Rect( 515 , 703 , 328 , 80)
    tipus_sq = pygame.Rect( 1006 , 580 , 280 , 80)
    tipus = False
    going = True
    tuto_board = False
    while going:
        delta=clock.tick(FPS)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                going = False
                result = GAME_STATE_EXIT
            if event.type == pygame.MOUSEBUTTONDOWN:
# Escollir el que vols en el menu principal
                if tipus==False:
                    if play_sq.collidepoint(pygame.mouse.get_pos()):
                        if game["tipus"] ==  1 or  game["tipus"] ==  2:
                            going = False
                            result = GAME_STATE_PLAYING
                    if tutorial_sq.collidepoint(pygame.mouse.get_pos()):
                        elapsed_time = 0
                        tuto_board = True
                    if tipus_sq.collidepoint(pygame.mouse.get_pos()):
                        tipus = True
                    if end_sq.collidepoint(pygame.mouse.get_pos()):
                        going = False
                        result = GAME_STATE_EXIT
                else:
                    if tipus_ores.collidepoint(pygame.mouse.get_pos()):
                        tipus = False
                        game["tipus"] =  1
                    elif tipus_mob.collidepoint(pygame.mouse.get_pos()):
                        tipus = False
                        game["tipus"] =  2
                    else:
                        tipus = False
                        game["tipus"] = 0 
        screen.blit(imatges["menu"], imatges["menu"].get_rect())
# Escollir el tipus de joc
        if tuto_board and elapsed_time < 5000:
            if game["tipus"] == 1:
                 elapsed_time+=delta
                 screen.blit(imatges["tutorialores"], imatges["tutorialores"].get_rect().move(510,265))
            elif game["tipus"] == 2:
                 elapsed_time+=delta
                 screen.blit(imatges["tutorialmob"], imatges["tutorialmob"].get_rect().move(510,265))
            else: 
                elapsed_time+=delta
                screen.blit(imatges["tutorial"], imatges["tutorial"].get_rect().move(510,265))
        if tipus:
            tipus_ores = pygame.Rect( 185 , 330 , 294 , 316)
            tipus_mob = pygame.Rect( 1058 , 285 , 202 , 333)
            screen.blit(imatges["tipustria"], imatges["tipustria"].get_rect())
        pygame.display.flip()       
    return result


#------------------------
#----------MAIN----------
#------------------------
def main():
    game={
    "tipus": 0
    }
    imatges = img()
    pygame.init()
    screen = pygame.display.set_mode([WINDOW_SIZE_X, WINDOW_SIZE_Y])
    game_icon = pygame.image.load('images/icon/Icon.png')
    
    pygame.display.set_caption('ExerciciFinal AlbertoG')
    pygame.display.set_icon(game_icon)
    game_state=GAME_STATE_MENU
    while game_state!=GAME_STATE_EXIT:
        if game_state==GAME_STATE_MENU:
            game_state=game_menu(screen, game,imatges)
        elif game_state==GAME_STATE_PLAYING:
            game_state=game_playing(screen, game,imatges)
    pygame.quit()

main()